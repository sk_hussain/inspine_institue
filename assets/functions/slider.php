<?php

add_shortcode('home_page_slider', 'shortcode_home_page_slider');

function shortcode_home_page_slider($atts, $content = null) {
    extract( shortcode_atts( array(
	// 	  'variable_1'	=> '',
    //   'variable_2'	=> '',
    //   'variable_3'	=> '',
      
	), $atts ) );
    ob_start();
?>


<div class="slider_home_page owl-carousel owl-theme">
<?php
// check if the repeater field has rows of data
if( have_rows('slider_home_page',option) ):

 	// loop through the rows of data
    while ( have_rows('slider_home_page',option) ) : the_row(); ?>

       <?php $img_data = get_sub_field('image_slider',option);?>
        <div class="item slider_item" style="background-image:url(<?php echo $img_data['url']?>);">
        <?php the_sub_field('description_slider', option); ?>
        
        <!-- <img src="<?php echo $img_data['url']?>" alt=""> -->
       </div>


<?php    endwhile;
endif;

?>

</div>

<script>
    jQuery(document).ready(function($) {

  $(".slider_home_page").owlCarousel({
    loop: true,
    autoplay:true,
    autoplayHoverPause:true,
    nav: false,
    dots: true,
    items: 1
  });
});

</script>

<?php

    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [home_page_slider][/home_page_slider]  **/