<?php

add_shortcode('video_banner', 'shortcode_video_banner');

function shortcode_video_banner($atts, $content = null) {
    extract( shortcode_atts( array(
	// 	  'variable_1'	=> '',
    //   'variable_2'	=> '',
    //   'variable_3'	=> '',
      
	), $atts ) );
    ob_start();
?>


<div class="video-banner-container">

    <div class="video-caption">
            <?php the_field('caption_video', 'option');?>
    </div>
    <div class="clearfix"></div>
    <video poster="" id="hero-video" playsinline  loop>
      <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
      <source src="<?php the_field('url_for_webm', option)?>" type="video/webm">
      <source src="<?php the_field('url_for_mp4', option)?>" type="video/mp4">
    </video>
</div>


<script> 
var myVideo = document.getElementById("hero-video"); 

function playPause() { 
    if (myVideo.paused) 
        myVideo.play(); 
    else 
        myVideo.pause(); 
} 

</script> 



<script>
    jQuery(document).ready(function($) {

  $(".slider_home_page").owlCarousel({
    loop: true,
    nav: false,
    dots: true,
    items: 1
  });
});

</script>

<?php

    $content_data = ob_get_contents();
    ob_end_clean();
    return $content_data;
}

/** [video_banner][/video_banner]  **/