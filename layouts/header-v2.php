<?php

?>

  <div id="skh-version__2">
    <nav class="menu menu--is-fixed">
      <div class="menu--wrapper">
      <div>
        <div class="col-sm-1 sqr__logo">
           <div class="menu__brand">
          <?php
                $logo_img = get_field('logo_header', 'option');
                if (!empty($logo_img)):
                ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                        <?php else : {
                ?>
                        <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                        <?php
                }
                endif;
                ?>
        </div>
        </div>
        <div class="col-sm-11 top-header">
          <div class="top-search">
            <?php echo do_shortcode('[wd_asp id=1]'); ?>
            <span class="contact_details"> 
            <h3 id="contact_number"><a href="tel:<?php the_field('phone_number', option);?>"><i class="fa fa-phone"></i> <?php the_field('phone_number', option);?></a></h3>
            <h3 id="contact_text"><a href="<?php the_field('contact_url', option);?>"><?php the_field('contact_text', option);?></a></h3>
            </span>

          </div>
           <div class="menu__list--wrapper">
          <?php
                wp_nav_menu(
                array(
                'theme_location' => 'header_menu',
                'container'       => false,
                'menu_id'      => '',
                'menu_class'         => 'menu__list',
                ));
                ?>
        </div>
        </div>
      </div>
       

       

      </div>

    </nav>

    <nav class="mobile-menu">

      <div class="mobile-menu--wrapper">
        <!-- <a href="#" class="anim-btn example5 toggleTopMenu-mobile"><span></span></a> -->

        <div class="mobile-menu__brand">
          <?php
                $logo_img = get_field('logo_header', 'option');
                if (!empty($logo_img)):
                ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo $logo_img['url']; ?>" alt="<?php echo $logo_img['alt']; ?>" /></a>
                        <?php else : {
                ?>
                        <h1 class="site-title"><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                        <?php
                }
                endif;
                ?>

        </div>


        <div class="mobile-menu__list--wrapper">
        <a href="#" class="anim-btn example5 toggleTopMenu-mobile"><span></span></a>
          <div class="mobile-menu__list">
          <div class="mobile_menu_wrap">


            <?php

                $menuParameters = array(
                  'theme_location' => 'mobile_menu',
                'container'       => false,
                'echo'            => false,
                'items_wrap'      => '%3$s',
                'depth'           => 0,
                );

                echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
                ?>
                </div>
          </div>


          <span class="overlay"></span>

        </div>




      </div>
    </nav>
  </div>


  <script type="text/javascript">
    jQuery(document).ready(function($) {

      $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll >= 350) {
          $('.menu--is-fixed').addClass("scroll");
          //console.log(scroll);

        } else {
          $('.menu--is-fixed').removeClass("scroll");
        }
      });


    

//       $(".menu-item-has-children").click(function() {
//         event.stopPropagation();
//         $('.menu-item-has-children .sub-menu').toggleClass("open-menu");
//       });

//       $(window).click(function() {
//         $('.menu-item-has-children .sub-menu').toggleClass("open-menu");
//        });


      $(".toggleTopMenu-mobile").click(function() {

        $('.mobile-menu__list').toggleClass("open-menu");
        $('.example5').toggleClass("show-animation");
        $('.overlay').toggleClass("open-menu");


      });


    });
  </script>